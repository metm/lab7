package ro.ubb.movierentals.serverremote;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ServerApp {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        "ro/ubb/movierentals/serverremote/config"
                );

        System.out.println("server starting...");
    }
}
