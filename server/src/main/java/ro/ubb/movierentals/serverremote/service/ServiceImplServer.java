package ro.ubb.movierentals.serverremote.service;

import common.Service;
import common.domain.Client;
import common.domain.Movie;
import common.domain.Rent;
import ro.ubb.movierentals.serverremote.repository.SQLClientRepo;
import ro.ubb.movierentals.serverremote.repository.SQLMovieRepo;
import ro.ubb.movierentals.serverremote.repository.SQLRentRepo;

public class ServiceImplServer implements Service {
    //@Qualifier("rmiProxyFactoryBean")

    //@Autowired
    private SQLMovieRepo movieDBRepo;
    //@Autowired
    private SQLClientRepo clientDBRepo;
    //@Autowired
    private SQLRentRepo rentDBRepo;


    public ServiceImplServer(SQLMovieRepo movie, SQLClientRepo client, SQLRentRepo rent){
        this.movieDBRepo = movie;
        this.clientDBRepo = client;
        this.rentDBRepo = rent;
    }


    @Override
    public String addMovie(Movie movie) {
        try{
            movieDBRepo.save(movie);
            return "Successfully added";}
        catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public Iterable<Movie> getAllMovies() {
        return movieDBRepo.findAll();

    }

    @Override
    public String removeMovie(Integer id) {
        try{
            movieDBRepo.deleteById(id);
            return "Successfully deleted";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String updateMovie(Movie newMovie) {
        try{
            movieDBRepo.update(newMovie);
            return "Successfully updated";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public Movie findMovie(Integer id) {
        return null;
    }

    @Override
    public String addClient(Client client) {
        try{
            clientDBRepo.save(client);
            return "Successfully added";}
        catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public Iterable<Client> getAllClients() {
        return clientDBRepo.findAll();
    }

    @Override
    public String removeClient(Integer id) {
        try{
            clientDBRepo.deleteById(id);
            return "Successfully deleted";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public String updateClient(Client newClient) {
        try{
            clientDBRepo.update(newClient);
            return "Successfully updated";
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public Client findClient(Integer id) {
        return null;
    }

    @Override
    public String addRent(Rent rent) {
        try{
            rentDBRepo.save(rent);
            return "Successfully added";}
        catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }

    @Override
    public Iterable<Rent> getAllRents() {
        return rentDBRepo.findAll();
    }

    @Override
    public Movie findMovieNameByID(Integer id) {
        return null;
    }
}
