package ro.ubb.movierentals.serverremote.config;

import common.Service;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import ro.ubb.movierentals.serverremote.repository.SQLClientRepo;
import ro.ubb.movierentals.serverremote.repository.SQLMovieRepo;
import ro.ubb.movierentals.serverremote.repository.SQLRentRepo;
import ro.ubb.movierentals.serverremote.service.ServiceImplServer;

@Configuration
public class ServerAppConfig {
//
        @Bean
        SQLMovieRepo movieSQLRepo(){return new SQLMovieRepo();}
//    //SQLClientRepo clientSQLRepo= new SQLClientRepo();
        @Bean
//    SQLMovieRepo movieSQLRepo = new SQLMovieRepo();
        SQLClientRepo clientSQLRepo(){return new SQLClientRepo();}
        @Bean
        SQLRentRepo rentSQLRepo(){return new SQLRentRepo();}
//    SQLRentRepo rentSQLRepo= new SQLRentRepo();

    @Bean
    Service clientService() {
        Service service = new ServiceImplServer(movieSQLRepo(),clientSQLRepo(),rentSQLRepo());
        return service;
    }

    @Bean
    RmiServiceExporter rmiServiceExporter() {
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceName("Service");
        exporter.setServiceInterface(Service.class);
        exporter.setService(clientService());
        return exporter;
    }

}
