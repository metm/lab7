package ro.ubb.movierentals.client.config;

import common.Service;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;
import ro.ubb.movierentals.client.service.ServiceImplClient;

@Configuration
public class ClientAppConfig {

//    @Bean
//    ServiceImplClient serviceImplClient(){return new ServiceImplClient();}

    @Bean
    Service serviceClient(){return new ServiceImplClient() {
    };}

    @Bean(name = "rmiProxyFactoryBean")
    RmiProxyFactoryBean rmiProxyFactoryBean(){
        RmiProxyFactoryBean proxy = new RmiProxyFactoryBean();
        proxy.setServiceInterface(Service.class);
        proxy.setServiceUrl("rmi://localhost:1099/Service");
        return proxy;
    }
}
