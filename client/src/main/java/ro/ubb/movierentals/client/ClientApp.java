package ro.ubb.movierentals.client;

import common.Service;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ro.ubb.movierentals.client.service.ServiceImplClient;
import ro.ubb.movierentals.client.ui.Console;

public class ClientApp {
    public static void main(String[] args){
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        "ro/ubb/movierentals/client/config"
                );

        /*ServiceImplClient serviceClient = context
                .getBean(ServiceImplClient.class);
        serviceClient.getAllMovies()
                .forEach(movie -> System.out.println(movie));*/

        ServiceImplClient serviceImplClient=context.getBean(ServiceImplClient.class);

        Console ui=new Console(serviceImplClient);
        ui.runConsole();

        System.out.println("bye client");
    }
}
